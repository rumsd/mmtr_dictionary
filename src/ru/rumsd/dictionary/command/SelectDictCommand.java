package ru.rumsd.dictionary.command;

import ru.rumsd.dictionary.Console;
import ru.rumsd.dictionary.controller.DictionaryController;
import ru.rumsd.dictionary.model.Dictionary;

public class SelectDictCommand implements Command {
    private DictionaryController controller;

    public SelectDictCommand(DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        Console.writeString("Выберите словарь:");
        Console.writeString("1 - Словарь 5 цифр");
        Console.writeString("2 - Словарь 4 латинские буквы");
        while (true) {
            int num = Console.readInt();
            if (num == 1) {
                checkChangedBeforeSelect();
                controller.setDictionary(new Dictionary(
                        "five_digit.txt", "^([0-9]{5})$", "5 цифр"
                ));
                break;
            } else if (num == 2) {
                checkChangedBeforeSelect();
                controller.setDictionary(new Dictionary(
                        "four_letter.txt", "^([a-zA-Z]{4})$", "4 латинские буквы"
                ));
                break;
            } else {
                Console.writeString("Введите число 1 или 2");
            }
        }
    }

    private void checkChangedBeforeSelect() {
        if (controller.checkChanged() && Console.saveQuestion()){
            controller.save();
        }
    }

    @Override
    public String toString() {
        return "Выбрать словарь";
    }
}
