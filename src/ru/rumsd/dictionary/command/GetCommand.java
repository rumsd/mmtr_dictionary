package ru.rumsd.dictionary.command;

import ru.rumsd.dictionary.Console;
import ru.rumsd.dictionary.controller.DictionaryController;

public class GetCommand implements Command {
    private DictionaryController controller;

    public GetCommand(DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        Console.writeString("Введите слово для поиска: ");
        String key = Console.readString();
        controller.get(key);
    }

    @Override
    public String toString() {
        return "Найти запись";
    }
}
