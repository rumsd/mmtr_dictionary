package ru.rumsd.dictionary.command;

import ru.rumsd.dictionary.Console;
import ru.rumsd.dictionary.controller.DictionaryController;

public class ExitCommand implements Command {
    private DictionaryController controller;

    public ExitCommand(DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        if (controller.checkChanged()) {
            if (Console.saveQuestion()) {
                controller.save();
            }
        }
        System.exit(0);
    }

    @Override
    public String toString() {
        return "Выход";
    }
}
