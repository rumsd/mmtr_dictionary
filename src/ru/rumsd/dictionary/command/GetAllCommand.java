package ru.rumsd.dictionary.command;

import ru.rumsd.dictionary.controller.DictionaryController;

public class GetAllCommand implements Command {
    private DictionaryController controller;

    public GetAllCommand(DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        controller.getAll();
    }

    @Override
    public String toString() {
        return "Отобразить все записи";
    }
}
