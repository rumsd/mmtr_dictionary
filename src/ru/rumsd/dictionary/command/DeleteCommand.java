package ru.rumsd.dictionary.command;

import ru.rumsd.dictionary.Console;
import ru.rumsd.dictionary.controller.DictionaryController;

public class DeleteCommand implements Command {
    private DictionaryController controller;

    public DeleteCommand(DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        Console.writeString("Введить слово для удаления: ");
        String key = Console.readString();
        controller.delete(key);
    }

    @Override
    public String toString() {
        return "Удалить запись";
    }
}
