package ru.rumsd.dictionary.command;

public interface Command {
    void execute();
}
