package ru.rumsd.dictionary.model;

public enum Action {
    CREATE,
    UPDATE,
    DELETE
}
