package ru.rumsd.dictionary.controller;

import ru.rumsd.dictionary.model.Dictionary;
import ru.rumsd.dictionary.view.View;


public class DictionaryController {
    private View view;
    private Dictionary dictionary;

    public DictionaryController(View view, Dictionary dictionary) {
        this.view = view;
        this.dictionary = dictionary;
    }

    public void create(String key, String value) {
        view.renderStatus(dictionary.create(key.trim(), value.trim()));
    }

    public void update(String key, String value) {
        view.renderStatus(dictionary.update(key.trim(), value.trim()));
    }

    public void delete(String key) {
        view.render("Удаление слова: " + key);
        view.renderStatus(dictionary.delete(key));
    }

    public void get(String key) {
        String value = dictionary.get(key);
        view.render(value == null ? key + " не найдено в словаре" : key + " означает: " + value);
    }

    public void getAll() {
        view.render(dictionary.get());
    }

    public void save() {
        view.render("Сохранение в словарь...");
        dictionary.save();
    }

    public void setDictionary(Dictionary dictionary, boolean save) {
        if (save) {
            save();
        }
        this.dictionary = dictionary;
    }

    public void setDictionary(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public boolean checkChanged() {
        if (dictionary == null){
            return false;
        }
        return this.dictionary.isChanged();
    }
}