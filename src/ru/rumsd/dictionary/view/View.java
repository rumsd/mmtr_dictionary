package ru.rumsd.dictionary.view;

import ru.rumsd.dictionary.model.Status;

import java.util.Map;

public interface View {
    void render(String line);

    void render(Map<String, String> data);

    void renderStatus(Status status);
}
