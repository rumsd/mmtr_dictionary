package ru.rumsd.dictionary.view;

import ru.rumsd.dictionary.Console;
import ru.rumsd.dictionary.model.Status;

import java.util.Map;

public class SimpleConsoleView implements View {

    @Override
    public void render(String line) {
        printBefore();
        Console.writeString(line);
        printAfter();
    }

    @Override
    public void render(Map<String, String> data) {
        printBefore();
        for (Map.Entry<String, String> entry : data.entrySet()) {
            Console.writeString(entry.getKey() + ": " + entry.getValue());
        }
        printAfter();
    }

    @Override
    public void renderStatus(Status status) {
        printBefore();
        System.out.println(status.getMessage());
        printAfter();
    }

    private void printAfter() {
        Console.writeString("");
        Console.writeString("*************************************");
        Console.writeString("");
    }

    private void printBefore() {
        Console.writeString("*************************************");
        Console.writeString("");
    }
}
