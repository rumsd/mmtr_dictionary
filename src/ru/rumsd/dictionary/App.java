package ru.rumsd.dictionary;

import ru.rumsd.dictionary.command.*;
import ru.rumsd.dictionary.controller.DictionaryController;
import ru.rumsd.dictionary.model.Dictionary;
import ru.rumsd.dictionary.view.SimpleConsoleView;
import ru.rumsd.dictionary.view.View;

import java.util.Map;
import java.util.TreeMap;

public class App {

    public static void main(String[] args) {
        Dictionary defaultDict = null;
        View view = new SimpleConsoleView();
        DictionaryController controller = new DictionaryController(view, defaultDict);
        new SelectDictCommand(controller).execute();


        Map<Integer, Command> mainMenu = new TreeMap<>();
        mainMenu.put(1, new GetCommand(controller));
        mainMenu.put(2, new CreateCommand(controller));
        mainMenu.put(3, new UpdateCommand(controller));
        mainMenu.put(4, new DeleteCommand(controller));
        mainMenu.put(5, new GetAllCommand(controller));
        mainMenu.put(6, new SelectDictCommand(controller));
        mainMenu.put(7, new ExitCommand(controller));

        while (true) {
            for (Map.Entry<Integer, Command> command : mainMenu.entrySet()) {
                Console.writeString(command.getKey() + " - " + command.getValue());
            }
            int comandNum = Console.readInt();
            if (mainMenu.containsKey(comandNum)) {
                mainMenu.get(comandNum).execute();
            }
        }

    }
}
